package com.epam.book.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.epam.book.entity.Book;
import com.epam.book.exception.BookNotFoundException;
import com.epam.book.service.impl.BookServiceImpl;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(BookController.class)
public class ControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private BookServiceImpl bookService;

	@Test
	void testGetBooks_status200() throws Exception {
		when(this.bookService.getAllBooks()).thenReturn(Collections.emptyList());
		this.mvc.perform(get("/books").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON));
	}

	@Test
	void testGetBookById_status200() throws Exception {
		Book book = new Book(1L, "nameA", "genre1");

		when(this.bookService.getBookById(1L)).thenReturn(book);

		MvcResult mvcResult = mvc
				.perform(MockMvcRequestBuilders.get("/books/1").accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		assertEquals(200, mvcResult.getResponse().getStatus());
		assertEquals(true, mvcResult.getResponse().getContentAsString().contains("nameA"));

	}

	@Test
	void bookNotFound_Status404() throws Exception {
		when(this.bookService.getBookById(1L)).thenThrow(new BookNotFoundException("Book not found"));

		this.mvc.perform(get("/books/1")).andExpect(status().isNotFound());
	}

}