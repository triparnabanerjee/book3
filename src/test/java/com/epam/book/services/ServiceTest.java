package com.epam.book.services;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.epam.book.dao.BookRepository;
import com.epam.book.entity.Book;
import com.epam.book.exception.BookNotFoundException;
import com.epam.book.service.BookService;
import com.epam.book.service.impl.BookServiceImpl;

@ExtendWith(SpringExtension.class)
public class ServiceTest {

	@MockBean
	private BookRepository bookRepository;

	BookService bookService;

	@BeforeEach
	public void before() {
		bookService = new BookServiceImpl(bookRepository);
	}

	@Test
	void testFindAllBooks() {

		Book bookObject = new Book("nameA", "genre1");
		List<Book> bookList = new ArrayList<>();
		bookList.add(bookObject);
		when(bookRepository.findAll()).thenReturn(bookList);

		List<Book> books = bookService.getAllBooks();
		assertEquals(1, books.size());
		assertEquals("nameA", books.get(0).getName());
		assertEquals("genre1", books.get(0).getGenre());

	}

	@Test
	void testFindById() {
		Book bookObject = new Book("nameA", "genre1");

		when(this.bookRepository.findById(bookObject.getId())).thenReturn(Optional.of(bookObject));

		Book book = bookService.getBookById(bookObject.getId());

		assertEquals(bookObject.getId(), book.getId());
		assertEquals("nameA", book.getName());
		assertEquals("genre1", book.getGenre());

	}

	@Test
	void testBookNotFound() {
		when(this.bookRepository.findById(1L)).thenReturn(Optional.empty());
		
		assertThrows(BookNotFoundException.class, () -> bookService.getBookById(1L));
	

	}

	@Test
	void testAddBook() {
		when(bookRepository.save(any()))
		.thenReturn(new Book(1L,"nameA", "genre1"));
		Book book = new Book(1L,"nameA", "genre1");
		Book newBook = bookService.addBook(book);

		assertEquals(book.getId(), newBook.getId());
		assertEquals("nameA", newBook.getName());
		assertEquals("genre1", newBook.getGenre());
	}

	@Test
	void testUpdate() {
		Book bookObject = new Book("nameA", "genre1");
		when(this.bookRepository.findById(bookObject.getId())).thenReturn(Optional.of(bookObject));
		Book newbookObject = new Book("newnameA", "newgenre1");
		when(this.bookRepository.save(any(Book.class))).thenReturn(newbookObject);

		Book updatedBook = bookService.updateBook(bookObject.getId(), newbookObject);

		assertEquals(newbookObject.getId(), updatedBook.getId());
		assertEquals("newnameA", updatedBook.getName());
		assertEquals("newgenre1", updatedBook.getGenre());
	}

	@Test
	void testNoBooktoUpdate() {
		when(this.bookRepository.findById(1L)).thenReturn(Optional.empty());

		assertThrows(BookNotFoundException.class, () -> bookService.updateBook(1L, new Book()));
	}

	@Test
	void testDelete() {
		when(this.bookRepository.save(any(Book.class))).thenReturn(new Book(1L,"nameA", "genre1"));
		Book book = new Book("nameA", "genre1");
		Book newBook = bookService.addBook(book);
		List<Book> bookList = new ArrayList<>();
		bookList.add(newBook);
		when(this.bookRepository.findAll()).thenReturn(bookList);
		when(this.bookRepository.findById(newBook.getId())).thenReturn(Optional.of(newBook));
		//willDoNothing().given(this.bookRepository).delete(any(Book.class));
		
		
		bookService.deleteById(newBook.getId());
		bookList.remove(newBook);
		List<Book> books = bookService.getAllBooks();
		assertEquals(0, books.size());
		
	}

	@Test
	void testBooknotFoundToDelete() {
		when(this.bookRepository.findById(1L)).thenReturn(Optional.empty());
		assertThrows(BookNotFoundException.class, () -> bookService.deleteById(1L));
		//assertEquals(false, bookService.deleteById(1L));
	}
}