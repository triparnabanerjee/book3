package com.epam.book.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import com.epam.book.dao.BookRepository;

import com.epam.book.entity.Book;

import java.util.List;

@DataJpaTest
public class RepositoryTest {

	@Autowired
	BookRepository bookRepository;

	@Autowired
	TestEntityManager entityManager;

	@BeforeEach
	void setup() {
		this.entityManager.persist(new Book("nameA", "genre1"));
		this.entityManager.persist(new Book("nameB", "genre2"));
	}

	@Test
	void testFindAll() {
		final List<Book> bookList = this.bookRepository.findAll();
		assertEquals(2, bookList.size());
	}

	@Test
	void testSave() {
		Book book = bookRepository.save(new Book("nameC", "genre3"));
		assertEquals(true, bookRepository.findById(book.getId()).isPresent());
	}
}