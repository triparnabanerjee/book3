package com.epam.book.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;


@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.NOT_FOUND)
	public class BookNotFoundException extends RuntimeException {
	  public BookNotFoundException(String message) {
	    super(message);
	  }

}