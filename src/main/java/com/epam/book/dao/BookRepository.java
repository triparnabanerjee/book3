package com.epam.book.dao;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.epam.book.entity.Book;
//@RepositoryRestResource
public interface BookRepository extends JpaRepository<Book,Long>{

}
