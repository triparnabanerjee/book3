package com.epam.book.service.impl;

import java.util.List;
import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.book.dao.BookRepository;
import com.epam.book.entity.Book;
import com.epam.book.exception.BookNotFoundException;
import com.epam.book.service.BookService;
@Service
public class BookServiceImpl implements BookService {

	@Autowired
	  public BookServiceImpl(BookRepository bookRepository) {
	    this.bookRepository = bookRepository;
	  }

	@Autowired
	BookRepository bookRepository;
	@Override
	public List<Book> getAllBooks() {
		
		return bookRepository.findAll();
	}

	@Override
	public Book getBookById(Long id) {
		return bookRepository.findById(id)
				.orElseThrow(bookNotFound(id));
	}

	 private Supplier<BookNotFoundException> bookNotFound(Long id) {
		    return () -> new BookNotFoundException("Book not found: " + id);
		  }

	@Override
	public Book addBook(Book book) {
		bookRepository.save(book);
		return book;

	}

	@Override
	public Book updateBook(Long id,Book book) {
	return bookRepository.findById(id)
		.map(newBook->bookRepository.save(book))
		.map(Book:: new)
		.orElseThrow(bookNotFound(id));
	}

	@Override
	public boolean deleteById(Long id) {
		bookRepository.findById(id)
		.map(Book:: new)
		.orElseThrow(bookNotFound(id));
		try {			
			bookRepository.deleteById(id);
			return true;
			
		} catch (BookNotFoundException e) {
			bookNotFound(id);
			return false;

		}

	}
	

}
