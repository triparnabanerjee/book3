package com.epam.book.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.epam.book.entity.Book;
@Service
public interface BookService {
List<Book> getAllBooks();
Book getBookById(Long id);
Book addBook(Book book);
Book updateBook(Long id,Book book);
boolean deleteById(Long id);
}
