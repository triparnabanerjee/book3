package com.epam.book.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Book {
	String name;
	String genre;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	public Book() {
		
	}
	public Book( Long id, String name, String genre) {
		super();
		this.name = name;
		this.genre = genre;
		this.id = id;
	}
	
	public Book(String name, String genre) {
		super();
		this.name = name;
		this.genre = genre;
	}
	public Book(Book book) {
	    this.id = book.getId();
	    this.name = book.getName();
	    this.genre = book.getGenre();
	  }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Book [name=" + name + ", genre=" + genre + ", id=" + id + "]";
	}

}
