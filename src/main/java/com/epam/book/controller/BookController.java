package com.epam.book.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.book.entity.Book;
import com.epam.book.service.BookService;
import com.epam.book.service.impl.BookServiceImpl;
@RequestMapping("/books")
@RestController
public class BookController {
@Autowired
BookServiceImpl bookService;
@GetMapping
public ResponseEntity<List<Book>> getBooks() {
	return ResponseEntity.ok(bookService.getAllBooks());
	
}
@GetMapping("/{id}")
public ResponseEntity<Book> getBookById(@PathVariable Long id){
	return ResponseEntity.ok(bookService.getBookById(id));
	
}
@PostMapping
public ResponseEntity<Book> addBook(@RequestBody Book book){
	Book bookCreate = bookService.addBook(book);
	
	 URI location = ServletUriComponentsBuilder.fromCurrentRequest()
	            .path("/{id}")
	            .buildAndExpand(bookCreate.getId())
	            .toUri();
	    return ResponseEntity
	            .created(location)
	            .body(bookCreate);
}
@PutMapping("/{id}")
public ResponseEntity<Book> updateBook(@PathVariable Long id, @RequestBody Book book){
	Book bookUpdate = bookService.updateBook(id, book);
	
	 URI location = ServletUriComponentsBuilder.fromCurrentRequest()
	            .path("/{id}")
	            .buildAndExpand(bookUpdate.getId())
	            .toUri();
	    return ResponseEntity
	            .created(location)
	            .body(bookUpdate);
	
}
@DeleteMapping("/{id}")
public ResponseEntity<?> delete(@PathVariable Long id){
	if(bookService.deleteById(id)) {
		return ResponseEntity.noContent().build();

	}
	else {
		return ResponseEntity.notFound().build();
	}
	
}

}
